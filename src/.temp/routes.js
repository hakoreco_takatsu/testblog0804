export default [
  {
    path: "/introduction-to-gridsome",
    component: () => import(/* webpackChunkName: "page--src--templates--post-vue" */ "/Users/takatsuhiroki/Desktop/blog/src/templates/Post.vue"),
    meta: {
      data: true
    }
  },
  {
    name: "404",
    path: "/404",
    component: () => import(/* webpackChunkName: "page--src--pages--404-vue" */ "/Users/takatsuhiroki/Desktop/blog/src/pages/404.vue")
  },
  {
    name: "home",
    path: "/",
    component: () => import(/* webpackChunkName: "page--src--pages--index-vue" */ "/Users/takatsuhiroki/Desktop/blog/src/pages/Index.vue")
  },
  {
    path: "/mac-pro-2019-review",
    component: () => import(/* webpackChunkName: "page--src--templates--post-vue" */ "/Users/takatsuhiroki/Desktop/blog/src/templates/Post.vue"),
    meta: {
      data: true
    }
  },
  {
    path: "/design-for-developers",
    component: () => import(/* webpackChunkName: "page--src--templates--post-vue" */ "/Users/takatsuhiroki/Desktop/blog/src/templates/Post.vue"),
    meta: {
      data: true
    }
  },
  {
    path: "/what-i-ate-for-breakfast",
    component: () => import(/* webpackChunkName: "page--src--templates--post-vue" */ "/Users/takatsuhiroki/Desktop/blog/src/templates/Post.vue"),
    meta: {
      data: true
    }
  },
  {
    path: "/freelance-vs-full-time-work",
    component: () => import(/* webpackChunkName: "page--src--templates--post-vue" */ "/Users/takatsuhiroki/Desktop/blog/src/templates/Post.vue"),
    meta: {
      data: true
    }
  },
  {
    path: "/vue-vs-react-comparison",
    component: () => import(/* webpackChunkName: "page--src--templates--post-vue" */ "/Users/takatsuhiroki/Desktop/blog/src/templates/Post.vue"),
    meta: {
      data: true
    }
  },
  {
    path: "/how-to-get-better-at-coding",
    component: () => import(/* webpackChunkName: "page--src--templates--post-vue" */ "/Users/takatsuhiroki/Desktop/blog/src/templates/Post.vue"),
    meta: {
      data: true
    }
  },
  {
    path: "/top-5-static-site-generators-in-vue",
    component: () => import(/* webpackChunkName: "page--src--templates--post-vue" */ "/Users/takatsuhiroki/Desktop/blog/src/templates/Post.vue"),
    meta: {
      data: true
    }
  },
  {
    path: "/test20190810",
    component: () => import(/* webpackChunkName: "page--src--templates--post-vue" */ "/Users/takatsuhiroki/Desktop/blog/src/templates/Post.vue"),
    meta: {
      data: true
    }
  },
  {
    path: "/blog/:page(\\d+)?",
    component: () => import(/* webpackChunkName: "page--src--pages--blog-vue" */ "/Users/takatsuhiroki/Desktop/blog/src/pages/Blog.vue"),
    meta: {
      data: true
    }
  },
  {
    path: "/tag/:id/:page(\\d+)?",
    component: () => import(/* webpackChunkName: "page--src--templates--tag-vue" */ "/Users/takatsuhiroki/Desktop/blog/src/templates/Tag.vue"),
    meta: {
      data: true
    }
  },
  {
    name: "*",
    path: "*",
    component: () => import(/* webpackChunkName: "page--src--pages--404-vue" */ "/Users/takatsuhiroki/Desktop/blog/src/pages/404.vue")
  }
]

