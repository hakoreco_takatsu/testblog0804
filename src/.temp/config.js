export default {
  "siteUrl": "https://hakoreco_takatsu.gitlab.io/",
  "siteName": "Gridsome Portfolio Starter",
  "titleTemplate": "%s - Gridsome Portfolio Starter",
  "siteDescription": "A simple portfolio theme for Gridsome powered by Tailwind CSS v1.0",
  "version": "0.6.7"
}